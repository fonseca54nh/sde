#include "LPC17xx.h"
#include "MCP23S17.h"
#include "digital.h"
#include "seg.h"

// Function to provide short delay

int main(void)
{
	SystemInit();
	
	SSeg_Config();

	LPC_GPIO4->FIOPIN = 0xf0000000;
	LPC_GPIO3->FIOPIN = 0x0f000000;

	// It breaks, lets call it windows
	MCP23S17Init();
	MCP23S17ConfigIO(0x8000, 0); //0b1000-0000-0000-0000
	MCP23S17ConfigPullUP(0x0000, 0); //0b0000-0000-0000-0000

	//init7Seg();

	pinMode( PIN( 1, 23 ), INPUT );
	int counter = 0;

	SSegWrite( counter );
	int mutex = 0;

	int bitLed = 0;

	while(1)
	{

		if( digitalRead( PIN( 1, 23 ) ) == 1 )
		{ 

			if( mutex == 0 )
			{
				mutex = 1;
				MCP23S17DigitalWrite8( 1, ~bitLed, 0);
			}
		}

		if( digitalRead( PIN( 1, 23 ) ) == 0 )
		{
			mutex = 0;
		}


		//apresenta( 12 );
	}
}

