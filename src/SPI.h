#ifndef SPI_H
#define SPI_H

#include <stdint.h>

void SPIInit();
void SPIWrite(uint8_t data);
uint8_t SPIRead();
void SPIEnable();
void SPIDisable();
uint8_t SPIWriteRead(uint8_t data);

#endif