#include "seg.h"

uint8_t display[]={63,6, 0x5b,0x4f,0x66,0x6d ,0x7d,0x07, 0x7f, 0x6f };
void apresenta (uint16_t numero)
{
    uint32_t disp[]={HABILITA_DISPLAY0,HABILITA_DISPLAY1,HABILITA_DISPLAY2,HABILITA_DISPLAY3};
    uint8_t x, d;
    uint16_t DIVISOR;
   

    DIVISOR = 1000;
    for (x=0;x<4;x++)
    {
        d = numero / DIVISOR;
        numero = numero % DIVISOR;
        DIVISOR = DIVISOR/10;
        mostra(disp[x],d);
    }
}

void mostra (uint32_t alvo, uint8_t caracter)
{
    uint8_t v;

    digitalWrite(HABILITA_DISPLAY0, LOW); 
    digitalWrite(HABILITA_DISPLAY1, LOW);  
    digitalWrite(HABILITA_DISPLAY2, LOW);  
    digitalWrite(HABILITA_DISPLAY3, LOW);  
 
    v = display[caracter];
    if (v & 1)      digitalWrite(SEGA, HIGH);
    else digitalWrite(SEGA, LOW);

    if ((v>>1) & 1) digitalWrite(SEGB, HIGH);
    else digitalWrite(SEGB, LOW);    
    
    if ((v>>2) & 1) digitalWrite(SEGC, HIGH);
    else digitalWrite(SEGC, LOW);

    if ((v>>3) & 1) digitalWrite(SEGD, HIGH);
    else digitalWrite(SEGD, LOW);

    if ((v>>4) & 1) digitalWrite(SEGE, HIGH);
    else digitalWrite(SEGE, LOW);

    if ((v>>5) & 1) digitalWrite(SEGF, HIGH);
    else digitalWrite(SEGF, LOW);

    if ((v>>6) & 1) digitalWrite(SEGG, HIGH);
    else digitalWrite(SEGG, LOW);  

    digitalWrite(alvo, HIGH);
    for (int x=0;x<10000;x++) asm("nop");

}

void init7Seg()
{
  pinMode(SEGA, OUTPUTD);
  pinMode(SEGB, OUTPUTD);
  pinMode(SEGC, OUTPUTD);
  pinMode(SEGD, OUTPUTD);
  pinMode(SEGE, OUTPUTD);
  pinMode(SEGF, OUTPUTD);
  pinMode(SEGG, OUTPUTD);
   
  pinMode(HABILITA_DISPLAY0, OUTPUTD);
  pinMode(HABILITA_DISPLAY1, OUTPUTD);
  pinMode(HABILITA_DISPLAY2, OUTPUTD);
  pinMode(HABILITA_DISPLAY3, OUTPUTD);
 
  digitalWrite(HABILITA_DISPLAY0, LOW);
  digitalWrite(HABILITA_DISPLAY1, LOW);
  digitalWrite(HABILITA_DISPLAY2, LOW);
  digitalWrite(HABILITA_DISPLAY3, LOW);
}

void short_delay (int n);

// Configurando os pinos que estão ligados aos leds
void SSeg_Config(void){

  LPC_GPIO0->FIODIR = 0x00000ff0;
  LPC_GPIO3->FIODIR = 0xffffffff;
  LPC_GPIO4->FIODIR = 0xffffffff;
}

void SSegWrite(unsigned char number){
	unsigned int SSegChar[] = {0xf3ff/*0*/,0xf06f/*1*/,0xf5bf/*2*/,0xf4ff/*3*/
							  ,0xf66f/*4*/,0xf6Df/*5*/,0xf7cf/*6*/,0xf07f/*7*/
							  ,0xf7ff/*8*/,0xf67f/*9*/,0xf77f/*A*/,0xf7cf/*B*/
							  ,0xf39f/*C*/,0xf5ef/*D*/,0xf79f/*E*/,0xf71f/*F*/};
	LPC_GPIO0->FIOPINL =  (SSegChar[number]);
}

void short_delay(int n)
{
   volatile int d;
   for (d=0; d<n*3000; d++){}
}

//int main() {
//  
//  SystemInit();
//
//  
//  pinMode(SEGA, OUTPUTD);
//  pinMode(SEGB, OUTPUTD);
//  pinMode(SEGC, OUTPUTD);
//  pinMode(SEGD, OUTPUTD);
//  pinMode(SEGE, OUTPUTD);
//  pinMode(SEGF, OUTPUTD);
//  pinMode(SEGG, OUTPUTD);
//   
//  pinMode(HABILITA_DISPLAY0, OUTPUTD);
//  pinMode(HABILITA_DISPLAY1, OUTPUTD);
//  pinMode(HABILITA_DISPLAY2, OUTPUTD);
//  pinMode(HABILITA_DISPLAY3, OUTPUTD);
// 
//  digitalWrite(HABILITA_DISPLAY0, LOW);
//  digitalWrite(HABILITA_DISPLAY1, LOW);
//  digitalWrite(HABILITA_DISPLAY2, LOW);
//  digitalWrite(HABILITA_DISPLAY3, LOW);
//  for(;;) {
//   apresenta(12);
//  
//
//  }
//}
