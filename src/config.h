#ifndef CONFIG_H
#define CONFIG_H

#define SPI_SSEL PIN(0, 21)
#define SPI_SCLK PIN(0, 15)
#define SPI_MISO PIN(0, 17)
#define SPI_MOSI PIN(0, 18)
#define SPI_PERIOD 1

#endif