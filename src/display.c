#include "display.h"
#include "delay.h"
#include "digital.h"
#include "periodica.h"

 uint8_t digitos[4];
 uint8_t mostra[4]={1,1,1,1};
 
 uint8_t disp[]={0x3f,0x06, 0x5b, 0x4f, 0x66, 0x6d,0x7d,0x07,0x7f,0x6f};
    uint8_t v[]={DISPLAY0,DISPLAY1,DISPLAY2,DISPLAY3};
 void mostraDisplay (uint8_t numero_display, uint8_t valor)
{

	
   
  digitalWrite(SEGA, HIGH);
  digitalWrite(SEGB, HIGH);
  digitalWrite(SEGC, HIGH);
  digitalWrite(SEGD, HIGH);
  digitalWrite(SEGE, LOW);
  digitalWrite(SEGF, LOW);
  digitalWrite(SEGG, HIGH);
  digitalWrite(DISPLAY0, LOW);
  digitalWrite(DISPLAY1, LOW);
  digitalWrite(DISPLAY2, LOW);
  digitalWrite(DISPLAY3, LOW);
  
  
  digitalWrite(v[numero_display], HIGH);
  
  digitalWrite(SEGA, disp[valor]&1);
  digitalWrite(SEGB, (disp[valor]>>1)&1);
  digitalWrite(SEGC, (disp[valor]>>2)&1);
  digitalWrite(SEGD, (disp[valor]>>3)&1);
  digitalWrite(SEGE, (disp[valor]>>4)&1);
  digitalWrite(SEGF, (disp[valor]>>5)&1);
  digitalWrite(SEGG, (disp[valor]>>6)&1);
  
}
void funcRefresh (void)
{
	static uint8_t v=0;
	
	static uint8_t podeOmitir = 1;

	

		if ((digitos[v] != 0) || (v==3)) podeOmitir=0;
		if (!podeOmitir) mostraDisplay(v,digitos[v]);
		

	v = (v + 1) % 4;
	if (v==0) 	podeOmitir =1;
}
 
void seg_init ( void )
{
  pinMode( SEGA, OUTPUT);
  pinMode( SEGB, OUTPUT);
  pinMode( SEGC, OUTPUT);
  pinMode( SEGD, OUTPUT);
  pinMode( SEGE, OUTPUT);
  pinMode( SEGF, OUTPUT);
  pinMode( SEGG, OUTPUT);
  pinMode(DISPLAY0, OUTPUT);
  pinMode(DISPLAY1, OUTPUT);
  pinMode(DISPLAY2, OUTPUT);
  pinMode(DISPLAY3, OUTPUT);
  
  for (uint8_t v =0; v<4;v++) digitos[v]=0;
  periodica_cria ("refresh",6, funcRefresh);
}

  
void seg_apresenta (uint16_t valor)
{
	for (uint8_t v=0;v<4;v++) digitos[v]=0;
	
	uint16_t divisor=1000;
	
	for (int a=0;a<4;a++)
	{
		digitos[a]=valor/divisor;
		valor = valor - digitos[a]*divisor;
		divisor=divisor/10;
	}
	
}

// definicoes do seg

#define LOW 0
#define HIGH 1

#define INPUT 0
#define OUTPUT 1

#define PIN0_00 ((0<<5)|0)
#define PIN0_01 ((0<<5)|1)
#define PIN0_02 ((0<<5)|2)
#define PIN0_03 ((0<<5)|3)

#define PIN1_08 ((1<<5)|8)


#define PIN1_23 ((1<<5)|23)
#define PIN1_24 ((1<<5)|24)
#define PIN1_25 ((1<<5)|25)
#define PIN1_26 ((1<<5)|26)
#define SW0 PIN1_23
#define SW1 PIN1_24
#define SW2 PIN1_25
#define SW3 PIN1_26

#define PIN0_04 ((0<<5)|4)
#define PIN0_05 ((0<<5)|5)
#define PIN0_06 ((0<<5)|6)
#define PIN0_07 ((0<<5)|7)
#define PIN0_08 ((0<<5)|8)
#define PIN0_09 ((0<<5)|9)
#define PIN0_10 ((0<<5)|10)


#define SEGA PIN0_04
#define SEGB PIN0_05
#define SEGC PIN0_06
#define SEGD PIN0_07
#define SEGE PIN0_08
#define SEGF PIN0_09
#define SEGG PIN0_10


#define PIN3_25 ((3<<5)|25)


#define PIN3_26 ((3<<5)|26)




#define PIN2_11 ((2<<5)|11)

#define PIN4_28 ((4<<5)|28)
#define PIN4_29 ((4<<5)|29)



