#include "MCP23S17.h"
#include "SPI.h"
#include "seg.h"
#include <stdint.h>

void MCP23S17Init()
{
	SPIInit();
	//Coloca todos IO como Input e dados salvos como 0
	for (uint8_t i = 0; i < 3; i++){
		MCP23S17WriteRegister(MCP23S17_IODIRA, 0xFF, i);
		MCP23S17WriteRegister(MCP23S17_GPIOA, 0x00, i);
	}
}

void MCP23S17WriteRegister(uint8_t addressRegister, uint8_t configuration, uint8_t addressSPI)
{
	SPIEnable();
	SPIWrite(MCP23S17_OPCODE_WRITE(addressSPI));
	SPIWrite(addressRegister);
	SPIWrite(configuration);
	SPIDisable();
}

uint8_t MCP23S17ReadRegister(uint8_t addressRegister, uint8_t addressSPI)
{
	SPIEnable();
	SPIWrite(MCP23S17_OPCODE_READ(addressSPI));
	SPIWrite(addressRegister);
	uint8_t returnData = SPIRead();
	SPIDisable();
  return returnData;
}

void MCP23S17ConfigIO(uint16_t configuration, uint8_t addressSPI)
{
	MCP23S17WriteRegister(MCP23S17_IODIRA, configuration >> 8, addressSPI);
	MCP23S17WriteRegister(MCP23S17_IODIRB, configuration, addressSPI);
}

void MCP23S17ConfigPINIO(uint8_t pin, uint8_t bit, uint8_t addressSPI)
{
	uint8_t temp;
	if (pin > 8) temp = MCP23S17ReadRegister(MCP23S17_IODIRA, addressSPI);
	else temp = MCP23S17ReadRegister(MCP23S17_IODIRB, addressSPI);
	temp = ((temp & ~(0x1 << pin)) | (bit << pin));
	MCP23S17ConfigIO(temp, addressSPI);
}

void MCP23S17ConfigPullUP(uint16_t configuration, uint8_t addressSPI)
{
	MCP23S17WriteRegister(MCP23S17_GPPUA, configuration >> 8, addressSPI);
	MCP23S17WriteRegister(MCP23S17_GPPUB, configuration, addressSPI);
}

void MCP23S17ConfigPINPullUP(uint8_t pin, uint8_t bit, uint8_t addressSPI)
{
	uint8_t temp;
	if (pin > 8) temp = MCP23S17ReadRegister(MCP23S17_GPPUA, addressSPI);
	else temp = MCP23S17ReadRegister(MCP23S17_GPPUB, addressSPI);
	temp = ((temp & ~(0x1 << pin)) | (bit << pin));
	MCP23S17ConfigIO(temp, addressSPI);
}

uint8_t MCP23S17DigitalRead8(uint8_t pos, uint8_t addressSPI)
{
  return MCP23S17ReadRegister((pos = MCP23S17_POSA ? MCP23S17_GPIOA : MCP23S17_GPIOB), addressSPI);
}

uint16_t MCP23S17DigitalRead16(uint8_t addressSPI)
{
  return
  (
    (MCP23S17DigitalRead8(MCP23S17_POSA, addressSPI) << 8) |
      MCP23S17DigitalRead8(MCP23S17_POSB, addressSPI)
  );
}

uint8_t MCP23S17DigitalReadPIN(uint8_t pin, uint8_t addressSPI)
{
	return ((MCP23S17DigitalRead16(addressSPI) >> pin) & 0x1);
}

void MCP23S17DigitalWrite8(uint8_t pos, uint8_t configuration, uint8_t addressSPI)
{
  MCP23S17WriteRegister((pos = MCP23S17_POSA ? MCP23S17_GPIOA : MCP23S17_GPIOB), configuration, addressSPI);
}

void MCP23S17DigitalWrite16(uint16_t configuration, uint8_t addressSPI)
{
	MCP23S17DigitalWrite8(MCP23S17_POSA, configuration >> 8, addressSPI);
	MCP23S17DigitalWrite8(MCP23S17_POSB, configuration, addressSPI);
}

void MCP23S17DigitalWritePIN(uint8_t pin, uint8_t bit, uint8_t addressSPI)
{
	uint16_t temp = MCP23S17DigitalRead16(addressSPI);
	temp = ((temp & ~(0x1 << pin)) | (bit << pin));
	MCP23S17DigitalWrite16(temp, addressSPI);
}
