#include "SPI.h"
#include "config.h"
#include "digital.h"
#include "delay.h"
#include "seg.h"

//PONTO CRÍTICO: VERIFICAR PROBLEMA COM
//INICIANDO ROTINA EM LOW OU HIGH PARA CLOCK

void SPIInit()
{
	SSegWrite( 1 );
	pinMode(SPI_SSEL, OUTPUT);
  	SSegWrite( 2 );
	pinMode(SPI_SCLK, OUTPUT);
	pinMode(SPI_MOSI, OUTPUT);
	pinMode(SPI_MISO, INPUT);
	digitalWrite(SPI_SSEL, HIGH);
	digitalWrite(SPI_SCLK, LOW);
}

void SPIWrite(uint8_t data)
{
	for(int8_t i = 7; i >= 0; i--)
	{
		SSegWrite( data );
		digitalWrite(SPI_MOSI, ((data >> i) & 0x1));
		delay_us(SPI_PERIOD);
		digitalWrite(SPI_SCLK, HIGH);
		delay_us(SPI_PERIOD);
		digitalWrite(SPI_SCLK, LOW);
	}
}

uint8_t SPIRead()
{
	uint8_t returnData = 0x0;
  for(int8_t i = 7; i >= 0; i--)
	{
		delay_us(SPI_PERIOD);
		digitalWrite(SPI_SCLK, HIGH);
		delay_us(SPI_PERIOD);
		digitalWrite(SPI_SCLK, LOW);
		returnData += digitalRead(SPI_MISO) << i;
	}
	return returnData;
}

uint8_t SPIWriteRead(uint8_t data)
{
	uint8_t returnData = 0x0;
	for(int8_t i = 7; i >= 0; i--)
	{
		digitalWrite(SPI_MOSI, ((data >> i) & 0x1));
		delay_us(SPI_PERIOD);
		digitalWrite(SPI_SCLK, HIGH);
		delay_us(SPI_PERIOD);
		digitalWrite(SPI_SCLK, LOW);
		returnData += digitalRead(SPI_MISO) << i;
	}
	return returnData;
}

void SPIEnable()
{
	digitalWrite(SPI_SSEL, LOW);
}

void SPIDisable()
{
	digitalWrite(SPI_SSEL, HIGH);
}
