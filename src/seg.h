#ifndef __SEG_H
#define __SEG_H

#include "LPC17xx.h"
#include "digital.h"

#define MEU_LED PIN4_29
#define BOTAO7   PIN1_08

#define BOTAO0 PIN1_23
#define BOTAO1 PIN1_24
#define BOTAO2 PIN1_25
#define BOTAO3 PIN1_26

#define HABILITA_DISPLAY0 PIN3_25
#define HABILITA_DISPLAY1 PIN3_26
#define HABILITA_DISPLAY2 PIN4_28
#define HABILITA_DISPLAY3 PIN4_29

void apresenta (uint16_t numero);
void mostra (uint32_t alvo, uint8_t caracter);
void init7Seg();

void short_delay (int n);
void SSeg_Config(void);
void SSegWrite(unsigned char number);
void short_delay(int n);

#endif
