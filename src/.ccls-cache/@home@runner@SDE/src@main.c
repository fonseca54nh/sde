#include "LPC17xx.h"
#include "lcd.c"
#include "ht1381.c"

int main(void) //Main function
{
    unsigned char s;

    SystemInit(); // Set up board/processor
    InitLCD(); // Inicializando o LCD
    OpenHT138x(); // Inicializando o HT1381

    WriteHT138x(SECONDS, 0x01);

    while(1)
    {
      SelLCDLine(0);
      s = ReadHT138x(SECONDS);
      WLCDPhrase("Segundos: ");
      WLCDChar((s >> 4)|0x30);
      WLCDChar((s & 0x0F)|0x30);
    }
    return 0 ;
}


