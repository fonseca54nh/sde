// Definição de comandos
#define SECONDS         0x80
#define MINUTES         0x82
#define HOURS           0x84
#define DATE            0x86
#define MONTH           0x88
#define DAY             0x8A
#define YEAR            0x8C
#define WPROTECT        0x8E

// Rotina de atraso por software
void Delay10TCYx(int n){
         volatile int d;
         for (d=0; d<n*10; d++);
}

unsigned char ReadHT138x(unsigned char registers){
	int i = 0;	
	unsigned char data = 0;

	LPC_GPIO0->FIODIR |= (1<<19);  // IO (P0.19)
	LPC_GPIO0->FIOPIN |= (1<<21);  // ChipSelect (P0.21)
	LPC_GPIO0->FIOPIN &= ~(1<<15); // CLK (P0.15)

	registers |= 0x01;		// comando de leitura

	Delay10TCYx(10);
	
	for(i=0; i<8; i++){
          if (registers & 0x01) LPC_GPIO0->FIOPIN |= (1<<19); // IO (P0.19)
          else LPC_GPIO0->FIOPIN &= ~(1<<19); // IO (P0.19)
          registers = registers >> 1;
          Delay10TCYx(1);
          LPC_GPIO0->FIOPIN |= (1<<15);  // CLK (P0.15)
          Delay10TCYx(1);
          LPC_GPIO0->FIOPIN &= ~(1<<15); // CLK (P0.15)
	}	
//
	LPC_GPIO0->FIODIR &= ~(1<<19); // IO (P0.19)
	Delay10TCYx(2);
//
	for(i=0; i<8; i++){
          Delay10TCYx(1);
          LPC_GPIO0->FIOPIN &= ~(1<<15); // CLK (P0.15)
          Delay10TCYx(1);
          LPC_GPIO0->FIOPIN |= (1<<15);  // CLK (P0.15)
          data = data >> 1;
          data = (LPC_GPIO0->FIOPIN & (1<<19)) ? (data | 0x80) : data; // IO (P0.19)
	}
	Delay10TCYx(5);
	LPC_GPIO0->FIOPIN &= ~(1<<21); // ChipSelect (P0.21)
	LPC_GPIO0->FIOPIN &= ~(1<<15); // CLK (P0.15)
	LPC_GPIO0->FIODIR |= (1<<19);  // IO (P0.19)
	Delay10TCYx(1);
	return(data);
}

void WriteHT138x(unsigned char registers, unsigned char data){

        int i = 0;

	LPC_GPIO0->FIODIR |= (1<<19);  // IO (P0.19)
	LPC_GPIO0->FIOPIN |= (1<<21);  // ChipSelect (P0.21)
	LPC_GPIO0->FIOPIN &= ~(1<<15); // CLK (P0.15)

	Delay10TCYx(1);
	
	for(i=0; i<8; i++){
          if (registers & 0x01) LPC_GPIO0->FIOPIN |= (1<<19); // IO (P0.19)
          else LPC_GPIO0->FIOPIN &= ~(1<<19); // IO (P0.19)
          registers = registers >> 1;
          Delay10TCYx(1);
          LPC_GPIO0->FIOPIN |= (1<<15);  // CLK (P0.15)
          Delay10TCYx(1);
          LPC_GPIO0->FIOPIN &= ~(1<<15); // CLK (P0.15)
	}	

	//LPC_GPIO0->FIODIR &= ~(1<<19);  // IO (P0.19)
	Delay10TCYx(1);

	for(i=0; i<8; i++){
		if (data & 0x01) LPC_GPIO0->FIOPIN |= (1<<19); // IO (P0.19)
		else LPC_GPIO0->FIOPIN &= ~(1<<19); // IO (P0.19)
		data = data >> 1;
		Delay10TCYx(1);
		LPC_GPIO0->FIOPIN |= (1<<15); // CLK (P0.15)
		Delay10TCYx(1);
		LPC_GPIO0->FIOPIN &= ~(1<<15); // CLK (P0.15)
	}	
	LPC_GPIO0->FIODIR &= ~(1<<19); // IO (P0.19)
	Delay10TCYx(1);
	LPC_GPIO0->FIOPIN &= ~(1<<21); // ChipSelect (P0.21)
}

void OpenHT138x(void){
        // Inicializa pinos de comunicação
        LPC_PINCON->PINMODE1 |= (1<<6);
        LPC_PINCON->PINMODE1 &= ~(1<<7);
        //LPC_PINCON->PINMODE_OD0 &= ~(1<<19);
        //LPC_PINCON->PINSEL2 = 0x00000000;

        // Desligando periféricos...
        LPC_SC->PCONP &= ~(0xffff0fff);
        LPC_SC->PCONP &= ~(1<<4);
        LPC_SC->PCONP &= ~(1<<19);

        LPC_GPIO0->FIODIR |= (1<<21);  // ChipSelect (P0.21)
        LPC_GPIO0->FIODIR |= (1<<15);  // CLK (P0.15)
        LPC_GPIO0->FIODIR &= ~(1<<19); // IO (P0.19)

        LPC_GPIO0->FIOPIN &= ~(1<<21); // ChipSelect (P0.21)
        LPC_GPIO0->FIOPIN &= ~(1<<15); // CLK (P0.15)

        WriteHT138x(WPROTECT, 0x00);    // Destrava a proteção de leitura
        WriteHT138x(SECONDS, 0x00);     // Ativa o oscilador interno
}
