#ifndef MCP23S17_H
#define MCP23S17_H

#include <stdint.h>

void MCP23S17Init();
void MCP23S17WriteRegister(uint8_t addressRegister, uint8_t configuration, uint8_t addressSPI);
uint8_t MCP23S17ReadRegister(uint8_t addressRegister, uint8_t addressSPI);
void MCP23S17ConfigIO(uint16_t configuration, uint8_t addressSPI);
void MCP23S17ConfigPINIO(uint8_t pin, uint8_t bit, uint8_t addressSPI);
void MCP23S17ConfigPullUP(uint16_t configuration, uint8_t addressSPI);
void MCP23S17ConfigPINPullUP(uint8_t pin, uint8_t bit, uint8_t addressSPI);
uint8_t MCP23S17DigitalRead8(uint8_t pos, uint8_t addressSPI);
uint16_t MCP23S17DigitalRead16(uint8_t addressSPI);
uint8_t MCP23S17DigitalReadPIN(uint8_t pin, uint8_t addressSPI);
void MCP23S17DigitalWrite8(uint8_t pos, uint8_t configuration, uint8_t addressSPI);
void MCP23S17DigitalWrite16(uint16_t configuration, uint8_t addressSPI);
void MCP23S17DigitalWritePIN(uint8_t pin, uint8_t bit, uint8_t addressSPI);

#define MCP23S17_IOCON_BANK 0

#define MCP23S17_OPCODE 0x4 //0100
#define MCP23S17_WRITE 0x0
#define MCP23S17_READ 0x1
#define MCP23S17_POSA 0
#define MCP23S17_POSB 1
#define OUTPUT 0
#define INPUT 1

#define MCP23S17_OPCODE_WRITE(Address) ((MCP23S17_OPCODE << 4) | (Address << 1) | MCP23S17_WRITE)
#define MCP23S17_OPCODE_READ(Address) ((MCP23S17_OPCODE << 4) | (Address << 1) | MCP23S17_READ)

#if MCP23S17_IOCON_BANK == 1
#define MCP23S17_IODIRA 0x00
#define MCP23S17_IODIRB 0x10
#define MCP23S17_IPOLA 0x01
#define MCP23S17_IPOLB 0x11
#define MCP23S17_GPINTENA 0x02
#define MCP23S17_GPINTENB 0x12
#define MCP23S17_DEFVALA 0x03
#define MCP23S17_DEFVALB 0x13
#define MCP23S17_INTCONA 0x04
#define MCP23S17_INTCONB 0x14
#define MCP23S17_IOCONA 0x05
#define MCP23S17_IOCONB 0x15
#define MCP23S17_GPPUA 0x06
#define MCP23S17_GPPUB 0x16
#define MCP23S17_INTFA 0x07
#define MCP23S17_INTFB 0x17
#define MCP23S17_CAPA 0x08
#define MCP23S17_CAPB 0x18
#define MCP23S17_GPIOA 0x09
#define MCP23S17_GPIOB 0x19
#define MCP23S17_OLATA 0x0A
#define MCP23S17_OLATB 0x1A
#else
#define MCP23S17_IODIRA 0x00
#define MCP23S17_IODIRB 0x01
#define MCP23S17_IPOLA 0x02
#define MCP23S17_IPOLB 0x03
#define MCP23S17_GPINTENA 0x04
#define MCP23S17_GPINTENB 0x05
#define MCP23S17_DEFVALA 0x06
#define MCP23S17_DEFVALB 0x07
#define MCP23S17_INTCONA 0x08
#define MCP23S17_INTCONB 0x09
#define MCP23S17_IOCONA 0x0A
#define MCP23S17_IOCONB 0x0D
#define MCP23S17_GPPUA 0x0C
#define MCP23S17_GPPUB 0x0D
#define MCP23S17_INTFA 0x07E
#define MCP23S17_INTFB 0x0F
#define MCP23S17_CAPA 0x10
#define MCP23S17_CAPB 0x11
#define MCP23S17_GPIOA 0x12
#define MCP23S17_GPIOB 0x13
#define MCP23S17_OLATA 0x14
#define MCP23S17_OLATB 0x15
#endif

#endif