#include "LPC17xx.h"
#include "MCP23S17.h"

int main(void)
{
  SystemInit();
	MCP23S17Init();
	MCP23S17ConfigIO(0x8000, 0); //0b1000-0000-0000-0000
	MCP23S17ConfigPullUP(0x0000, 0); //0b0000-0000-0000-0000
}