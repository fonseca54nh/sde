const unsigned char WebSide[] = {
"<html>\r\n"
"<head>\r\n"
"<meta http-equiv=\"refresh\" content=\"5\">\r\n"
"<title>HTTPServer - Site de teste do XM700</title>\r\n"
"</head>\r\n"
"\r\n"
"<body bgcolor=\"#3030A0\" text=\"#FFFF00\">\r\n"
"<p><b><font color=\"#FFFFFF\" size=\"6\"><i>Ol�! Veja Papai! Estou na rede!!!!</i></font></b></p>\r\n"
"\r\n"
"<p><b>This is a dynamic website hosted by the embedded Webserver</b> <b>easyWEB.</b></p>\r\n"
"<p><b>Traduzindo: Este site � din�mico e est� em um sistema embarcado easyWEB</b></p>\r\n"
"<p><b>Hardware:</b></p>\r\n"
"<ul>\r\n"
"<li><b>Kit Educacional Exsto XM700 baseado no NXP LPC1768</b></li>\r\n"
"<li><b>Ethernet DP83848VV</b></li>\r\n"
"<li><b>Comunica��o Serial RS232/RS485</b></li>\r\n"
"<li><b>Comunica��o Serial CAM</b></li>\r\n"
"<li><b>Display Alfanum�rico 16x2</b></li>\r\n"
"<li><b>Potenciometro Digital SPI</b></li>\r\n"
"<li><b>RTC Externo com Bateria</b></li>\r\n"
"<li><b>Keyboard</b></li>\r\n"
"<li><b>Quatro Displays de Sete Segmentos</b></li>\r\n"
"<li><b>Mem�ria I2C</b></li>\r\n"
"<li><b>Oito Leds e Oito Chaves Retentivas</b></li>\r\n"
"<li><b>USB Host/Device/OTG</b></li>\r\n"
"<li><b>Acesso ao SDCard SPI</b></li>\r\n"
"<li><b>Programador JTAG integrado compat�vel com o OpenOCD</b></li>\r\n"
"<li><b>Buzzer/Lampada/DAC Externo</b></li>\r\n"
"<li><b>ADC com leituras de 4-20mA, 0-3V3 e 0-10V</b></li>\r\n"
"</ul>\r\n"
"\r\n"
"</body>\r\n"
"</html>\r\n"
"\r\n"};
