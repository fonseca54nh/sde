/**
 * \file  main.c
 * \brief  Teste de hardware do XM700
 * \author Marcelo Martins Maia do Couto
 * \version 1.0
 * \date  Inicio: 25/Ago/2010
 * \date  Final: 25/Ago/2010 
 * \todo 
 * \li Site: www.exsto.com.br
 * \li Email: marcelo@exsto.com.br
 *
 *  (Objetivo) Validar o funcionamento do m�dulo Inputs do Projeto XM700
 */

#include "LPC17xx.h"

// ADOCR constants
#define START_ADC (1<<24)
#define OPERATIONAL_ADC (1<<21)
#define ADC_DONE_BIT (1<<31)

#include "lcd.c"

void short_delay(int n)
{
   volatile int d;
   for (d=0; d<n*3000; d++){}
}

// MAIN function
int main(void) {

  unsigned int adval, valor = 0, valor2, pulsos = 0;
  unsigned char tx[5], channel = 5;

  // Configurando uC
  SystemInit();

  // Inicializando LCD
  InitLCD();

  // Configurando o ADC
  LPC_SC->PCONP |= (1<<12);

  // Configurando a frequencia de clock do ADC
  LPC_SC->PCLKSEL0 &= ~(0x03000000);
  LPC_SC->PCLKSEL0 |= (0x3<<24);

  // Configurando o canal ADC5
  LPC_PINCON->PINSEL3 &= ~(0xC0000000); // Garantindo que o canal est� tudo zerado
  LPC_PINCON->PINSEL3 |= (0xC0000000);

  // Configurando aquecedor e cooler
  LPC_GPIO1->FIODIR |= (1<<29);
  LPC_GPIO1->FIODIR &= ~(1<<28);
  LPC_GPIO2->FIODIR |= (1<<11);

  // Ao infinito e Al�m!!!!
  while(1){

    LPC_ADC->ADCR = START_ADC | OPERATIONAL_ADC | (1<<channel); // Iniciar convers�o ADC

    do
    {
      adval = LPC_ADC->ADGDR;                // Read A/D Data Register
    } while ((adval & ADC_DONE_BIT) == 0);   // Wait for end of A/D Conversion

    LPC_ADC->ADCR &= ~(START_ADC | OPERATIONAL_ADC | (1<<channel));   // Terminar convers�o ADC

     // Obtendo o valor convertido em digital
    valor2 = (adval >> 4) & 0x0FFF ;
    valor2 = (3300 * valor2) / 4096; // Achando a tens�o medida do sensor de temperatura
    valor = (5 * valor2) / 100; // Transformando Tens�o para Temperatura (1V -> 50 graus para Vcc 3V)
    valor = valor - 15; // Tirando 0,3v em temperatuda do erro da conversao
    tx[0] = (valor / 10) + 0x30;
    tx[1] = (valor % 10) + 0x30;

    if(tx[0] > 0x34) LPC_GPIO1->FIOPIN |= (1<<29);
    else LPC_GPIO1->FIOPIN &= ~(1<<29);

    if (tx[0] < 0x35) LPC_GPIO2->FIOPIN |= (1<<11);
    else LPC_GPIO2->FIOPIN &= ~(1<<11);

    SelLCDLine(0); // Posicionando na linha 0
    WLCDPhrase("T: ");
    WLCDChar(tx[0]);
    WLCDChar(tx[1]);
    WLCDPhrase("oC");

    short_delay(500);

    // Enter an infinite loop, cycling through led flash sequence
    if(!(LPC_GPIO1->FIOPIN & (1<<28))) pulsos++;

    valor = pulsos;
    tx[0] = valor /10000 + 0x30;
    valor = valor % 10000;
    tx[1] = valor /1000 + 0x30;
    valor = valor % 1000;
    tx[2] = valor /100 + 0x30;
    valor = valor % 100;
    tx[3] = valor /10 + 0x30;
    tx[4] = valor % 10 + 0x30;

    SelLCDLine(1); // Posicionando na linha 0
    WLCDPhrase("P: ");
    WLCDChar(tx[0]);
    WLCDChar(tx[1]);
    WLCDChar(tx[2]);
    WLCDChar(tx[3]);
    WLCDChar(tx[4]);
  }
  return 0 ;
}



