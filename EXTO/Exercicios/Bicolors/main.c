/**
 * \file  main.c
 * \brief  Teste de hardware do XM700
 * \author Marcelo Martins Maia do Couto
 * \version 1.0
 * \date  Inicio: 25/Ago/2010
 * \date  Final: 25/Ago/2010 
 * \todo 
 * \li Site: www.exsto.com.br
 * \li Email: marcelo@exsto.com.br
 *
 *  (Objetivo) Validar o funcionamento do m�dulo Bicolor leds do Projeto XM700
 */

#include "LPC17xx.h"

void short_delay(int n)
{
   volatile int d;
   for (d=0; d<n*3000; d++){}
}

// MAIN function
int main(void) {
	
	// Set up board/processor
	SystemInit();

	// Configurando portais... 
	LPC_GPIO3->FIODIR |= (1<<26); //Habilitando o P3.26 como sa�da
	LPC_GPIO3->FIODIR |= (1<<25); // Habilitando o P3.25 como sa�da
	LPC_GPIO4->FIODIR |= (1<<28); // Habilitando o P4.28 como sa�da
	LPC_GPIO4->FIODIR |= (1<<29); // Habilitando o P4.29 como sa�da
	

	while(1) { // Ao infinito e Al�m!!!!!!
	  LPC_GPIO3->FIOPIN |= (1<<26);  // Ativando P3.26 como sa�da
	  LPC_GPIO3->FIOPIN &= ~(1<<25); // Desativando P3.25 como sa�da
	  LPC_GPIO4->FIOPIN |= (1<<28);  // Ativando P4.28 como sa�da
	  LPC_GPIO4->FIOPIN &= ~(1<<29); // Habilitando o P4.29 como sa�da
	  short_delay(1000);
	  LPC_GPIO3->FIOPIN &= ~(1<<26);  // Ativando P3.26 como sa�da
	  LPC_GPIO3->FIOPIN |= (1<<25); // Desativando P3.25 como sa�da
	  LPC_GPIO4->FIOPIN &= ~(1<<28);  // Ativando P4.28 como sa�da
	  LPC_GPIO4->FIOPIN |= (1<<29); // Habilitando o P4.29 como sa�da
	  short_delay(1000);
	}
	return 0 ;
}


