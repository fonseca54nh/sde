/**
 * \file  main.c
 * \brief  Teste de hardware do XM700
 * \author Marcelo Martins Maia do Couto
 * \version 1.0
 * \date  Inicio: 25/Ago/2010
 * \date  Final: 25/Ago/2010 
 * \todo 
 * \li Site: www.exsto.com.br
 * \li Email: marcelo@exsto.com.br
 *
 *  (Objetivo) Validar o funcionamento do m�dulo SWITCHS do Projeto XM700
 */

#include "LPC17xx.h"

// MAIN function
int main(void) {
	
	// Set up board/processor
	SystemInit();

	// Configurando portais... 
	LPC_GPIO0->FIODIR = 0x00000FF0; // Leds Output/Chaves Input
	LPC_GPIO1->FIODIR = 0x00000000; // Chaves Input
	
	// Enter an infinite loop, cycling through led flash sequence
	while(1) {
	  // CH7
	  if(LPC_GPIO1->FIOPIN & (1<<8)) LPC_GPIO0->FIOPIN |= (1<<11);
	  else LPC_GPIO0->FIOPIN &= ~(1<<11);
	  // CH6
	  if(LPC_GPIO1->FIOPIN & (1<<4)) LPC_GPIO0->FIOPIN |= (1<<10);
	  else LPC_GPIO0->FIOPIN &= ~(1<<10);
	  // CH5
	  if(LPC_GPIO1->FIOPIN & (1<<1)) LPC_GPIO0->FIOPIN |= (1<<9);
	  else LPC_GPIO0->FIOPIN &= ~(1<<9);
	  // CH4
	  if(LPC_GPIO1->FIOPIN & (0x1)) LPC_GPIO0->FIOPIN |= (1<<8);
	  else LPC_GPIO0->FIOPIN &= ~(1<<8);
	  // CH3
	  if(LPC_GPIO1->FIOPIN & (1<<26)) LPC_GPIO0->FIOPIN |= (1<<7);
	  else LPC_GPIO0->FIOPIN &= ~(1<<7);
	  // CH2
	  if(LPC_GPIO1->FIOPIN & (1<<25)) LPC_GPIO0->FIOPIN |= (1<<6);
	  else LPC_GPIO0->FIOPIN &= ~(1<<6);
	  // CH1
	  if(LPC_GPIO1->FIOPIN & (1<<24)) LPC_GPIO0->FIOPIN |= (1<<5);
	  else LPC_GPIO0->FIOPIN &= ~(1<<5);
	  // CH0
	  if(LPC_GPIO1->FIOPIN & (1<<23)) LPC_GPIO0->FIOPIN |= (1<<4);
	  else LPC_GPIO0->FIOPIN &= ~(1<<4);
	}
	return 0 ;
}


