/*
**************************************************************************************************************
*                                                 NXP USB Host Stack
*
*                                     (c) Copyright 2008, NXP SemiConductors
*                                     (c) Copyright 2008, OnChip  Technologies LLC
*                                                 All Rights Reserved
*
*                                                  www.nxp.com
*                                               www.onchiptech.com
*
* File           : usbhost_uart.c
* Programmer(s)  : Prasad.K.R.S.V
* Version        :
*
**************************************************************************************************************
*/

/*
**************************************************************************************************************
*                                           INCLUDE HEADER FILES
**************************************************************************************************************
*/

#include "usbhost_uart.h"

/*
**************************************************************************************************************
*                                         INITIALIZE UART
*
* Description: This function initializes UART port, setup pin select, clock, parity, stopbits, FIFO etc
*
* Arguments  : baud_rate    UART baud rate (115200)
*
* Returns    : None
*
**************************************************************************************************************
*/

void  UART_Init(int cclk, uint32_t baudrate)
{
    uint32_t  Fdiv;
          
	// Configurando a fun��o dos pinos como TXD e RXD
	LPC_PINCON->PINSEL0 &= ~(0x000000F0);
	LPC_PINCON->PINSEL0 |= (0x00000050);
	LPC_PINCON->PINMODE0 &= ~(0x000000F0);
	LPC_PINCON->PINMODE0 |= (0x00000050);
	// Turn on power to UART0
	LPC_SC->PCONP |=  (1<<3);
	// Turn on UART0 peripheral clock
	LPC_SC->PCLKSEL0 &= ~(1<<7); //
	LPC_SC->PCLKSEL0 &= ~(1<<6); // PCLK_UART0 = CCLK / 4 = 25MHz
	
    LPC_UART0->LCR = 0x83;		/* 8 bits, no Parity, 1 Stop bit */

    // Configurando a taxa da comunica��o serial
    if (LPC_SC->PCLKSEL0 & 0x0000000C) Fdiv = ( cclk / 8 ) / (16*baudrate);
    else if (LPC_SC->PCLKSEL0 & 0x00000004) Fdiv = ( cclk / 1 ) / (16*baudrate);
    else if (LPC_SC->PCLKSEL0 & 0x0000008) Fdiv = ( cclk / 2 ) / (16*baudrate);
    else Fdiv = ( cclk / 4 ) / (16*baudrate);

    LPC_UART0->DLM = Fdiv / 256;							
    LPC_UART0->DLL = Fdiv % 256;
    LPC_UART0->LCR = 0x03;		/* DLAB = 0 */
    LPC_UART0->FCR = 0x07;		/* Enable and reset TX and RX FIFO. */

}

/*
**************************************************************************************************************
*                                         PRINT CHARECTER
*
* Description: This function is used to print a single charecter through UART1.
*
* Arguments  : ch    charecter to be printed
*
* Returns    : None
*
**************************************************************************************************************
*/

void  UART_PrintChar (uint8_t ch)
{

   while (!(LPC_UART0->LSR & 0x20));
   LPC_UART0->THR  = ch;
}

/*
**************************************************************************************************************
*                                         PRINT STRING
*
* Description: This function is used to print a string
*
* Arguments  : str    Pointer to the string
*
* Returns    : None
*
**************************************************************************************************************
*/

void  UART_PrintStr (const uint8_t * str)
{

   while ((*str) != 0) {
      if (*str == '\n') {
         UART_PrintChar(*str++);
         UART_PrintChar('\r');
      } else {
         UART_PrintChar(*str++);
      }    
   }
}

/*
**************************************************************************************************************
*                                        PRINT FORMATTED STRING
*
* Description: This function is used to print formatted string. This function takes variable length arguments
*
* Arguments  : variable length arguments
*
* Returns    : None
*
**************************************************************************************************************
*/

void  UART_Printf (const  uint8_t *format, ...)
{
    static  uint8_t  buffer[40 + 1];
            va_list     vArgs;

    va_start(vArgs, format);
    vsprintf((char *)buffer, (char const *)format, vArgs);
    va_end(vArgs);
    UART_PrintStr((uint8_t *) buffer);
}
