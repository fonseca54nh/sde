/**
 * \file  main.c
 * \brief  Teste de hardware do XM700
 * \author Marcelo Martins Maia do Couto
 * \version 1.0
 * \date  Inicio: 25/Ago/2010
 * \date  Final: 25/Ago/2010 
 * \todo 
 * \li Site: www.exsto.com.br
 * \li Email: marcelo@exsto.com.br
 *
 *  (Objetivo) Validar o funcionamento do m�dulo LEDS do Projeto XM700  
 */

#include "LPC17xx.h"

// Function to provide short delay
void short_delay (int n) __attribute__((noinline));

void short_delay(int n)
{
   volatile int d;
   for (d=0; d<n*3000; d++){}
}

// MAIN function
int main(void) {
	
	// Set up board/processor
	SystemInit();

	// Configurando portais... 
	 LPC_GPIO1->FIODIR |= (1<<23);
	 LPC_GPIO1->FIODIR |= (1<<24);
	 LPC_GPIO1->FIODIR |= (1<<25);
	 LPC_GPIO1->FIODIR |= (1<<26);

	// Enter an infinite loop, cycling through led flash sequence
	while(1) {
		LPC_GPIO1->FIOPIN &= ~(1<<26);
		LPC_GPIO1->FIOPIN |= (1<<23);
		short_delay(2000);
		LPC_GPIO1->FIOPIN &= ~(1<<23);
		LPC_GPIO1->FIOPIN |= (1<<24);
		short_delay(2000);
		LPC_GPIO1->FIOPIN &= ~(1<<24);
		LPC_GPIO1->FIOPIN |= (1<<25);
		short_delay(2000);
		LPC_GPIO1->FIOPIN &= ~(1<<25);
		LPC_GPIO1->FIOPIN |= (1<<26);
		short_delay(2000);
	}
	return 0 ;
}


