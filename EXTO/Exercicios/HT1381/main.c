/**
 * \file  main.c
 * \brief  Teste de hardware do XM700
 * \author Marcelo Martins Maia do Couto
 * \version 1.0
 * \date  Inicio: 25/Ago/2010
 * \date  Final: 25/Ago/2010 
 * \todo 
 * \li Site: www.exsto.com.br
 * \li Email: marcelo@exsto.com.br
 *
 *  (Objetivo) Validar o funcionamento do m�dulo RealTime Clock do Projeto XM700
 */

#include "LPC17xx.h"
#include "lcd.c"
#include "ht1381.c"

int main(void) //Main function
{
    unsigned char s;

    SystemInit(); // Set up board/processor
    InitLCD(); // Inicializando o LCD
    OpenHT138x(); // Inicializando o HT1381

    WriteHT138x(SECONDS, 0x01);

    while(1)
    {
      SelLCDLine(0);
      s = ReadHT138x(SECONDS);
      WLCDPhrase("Segundos: ");
      WLCDChar((s >> 4)|0x30);
      WLCDChar((s & 0x0F)|0x30);
    }
    return 0 ;
}


