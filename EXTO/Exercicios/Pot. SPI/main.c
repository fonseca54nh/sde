/**
 * \file  main.c
 * \brief  Teste de hardware do XM700
 * \author Marcelo Martins Maia do Couto
 * \version 1.0
 * \date  Inicio: 25/Ago/2010
 * \date  Final: 25/Ago/2010 
 * \todo 
 * \li Site: www.exsto.com.br
 * \li Email: marcelo@exsto.com.br
 *
 *  (Objetivo) Validar o funcionamento do m�dulo LEDS do Projeto XM700  
 */

#include "LPC17xx.h"

#include "lcd.c"
#include "keyboard.c"
#include "mcp41010.c"

// ADOCR constants
#define START_ADC (1<<24)
#define OPERATIONAL_ADC (1<<21)
#define ADC_DONE_BIT (1<<31)

void short_delay(int n)
{
   volatile int d;
   for (d=0; d<n*3000; d++){}
}

// MAIN function
int main(void) {
  int adval, valor;
  unsigned char tecla, cont, tx[4];

  SystemInit(); // Setup board/processor

  // Inicializando o LCD
  InitLCD();

  // Inicializando o SPI
  InitMCP41010();

  // Inicializando Teclado
  InitKeyboard();

  // Configurando o ADC
  LPC_SC->PCONP |= (1 << 12);
  // Configurando a frequencia de clock do ADC
  LPC_SC->PCLKSEL0 &= ~(0x03000000);
  LPC_SC->PCLKSEL0 |=  (0x3<<24);
  // Configurando o canal
  LPC_PINCON->PINSEL3 &= ~((0x3)<<30); // Garantindo que o canal est� tudo zerado
  LPC_PINCON->PINSEL3 |= ((0x3)<<30);

  while(1) {
    tecla = RKeyboard();
    if(tecla == 1) cont++;
    if(tecla == 2) cont--;
    MCP41010(cont);
    short_delay(100);

    LPC_ADC->ADCR = START_ADC | OPERATIONAL_ADC | (1<<5); // Start A/D conversion for on AD0.0
    do { adval = LPC_ADC->ADGDR; } while ((adval & ADC_DONE_BIT) == 0);   // Wait for end of A/D Conversion
    LPC_ADC->ADCR &= ~(START_ADC | OPERATIONAL_ADC | (1<<5));   // Stop A/D Conversion

     // Obtendo o valor convertido em digital
    adval = (adval >> 4) & 0x0FFF ;

    valor = adval;
    valor = ((long) 3300 * valor)/4096;
    tx[0] = valor /1000 + 0x30;
    valor = valor % 1000;
    tx[1] = ',';
    tx[2] = valor /100 + 0x30;
    valor = valor % 100;
    tx[3] = valor /10 + 0x30;
    tx[4] = valor % 10 + 0x30;

    SelLCDLine(0); // Posicionando na linha 0
    WLCDPhrase("Tensao Potenc. D");
    WLCDChar(0x35);
    SelLCDLine(1); // Posicionando na linha 0
    WLCDPhrase("V: ");
    WLCDChar(tx[0]);
    WLCDChar(',');
    WLCDChar(tx[2]);
    WLCDChar(tx[3]);
    WLCDChar(tx[4]);
    WLCDChar('V');
  }
  return 0 ;
}


