/**
 * \file  mcp41010.c
 * \brief  Biblioteca de comandos do Potenciometro Digital
 * \author Marcelo Martins Maia do Couto
 * \version 1.0
 * \date  Inicio: 25/Ago/2010
 * \date  Final: 25/Ago/2010
 * \todo 
 * \li Adaptar todos os c�digos para o Doxygen....
 *
 *  (Inicio) Defini��es de comandos para o potenciomentro digital do kit XM700
 */

#define DPOT_W  0x1300 //0b00010011

void InitMCP41010(void){
  // Habilitando a Comunica��o SPI
  LPC_SC->PCONP |= (1<<8);
  LPC_SC->PCONP &= ~(1<<4);
  LPC_SC->PCONP &= ~(1<<21);
  // Configurando o clock para este perif�rico: pclk = cclk / 4
  LPC_SC->PCLKSEL0 &= ~(1<<16);
  LPC_SC->PCLKSEL0 &= ~(1<<17);
  // Dividindo a frequencia do PCLK por 25, assim o clock da SPI � de 1MHz
  LPC_SPI->SPCCR = 25;
  // Configurando os modos dos pinos SPI
  LPC_PINCON->PINSEL0 |= 0xC0000000; // P0.15 (SCK)
  LPC_PINCON->PINSEL1 |= 0x00000030; // P0.18 (MOSI) e P0.16 (GPIO)
  // Configurando os registros de controle do SPI
  LPC_SPI->SPCR = 0x0024; // Latch (Rising) - Clockout (falling)
  // Configurando P0.16 como Sa�da
  LPC_PINCON->PINMODE1 |= 0x02;
  LPC_GPIO0->FIODIR |= (1<<16);
}

/*--------------------------------------------------------------*/
void MCP41010(unsigned char data)
{
  LPC_GPIO0->FIOPIN &= ~(1<<16);
  LPC_SPI->SPDR = DPOT_W | data;
  while(!(LPC_SPI->SPSR & 0x80));
  LPC_GPIO0->FIOPIN |= (1<<16);
}