/**
 * \file  main.c
 * \brief  Teste de hardware do XM700
 * \author Marcelo Martins Maia do Couto
 * \version 1.0
 * \date  Inicio: 25/Ago/2010
 * \date  Final: 25/Ago/2010 
 * \todo 
 * \li Site: www.exsto.com.br
 * \li Email: marcelo@exsto.com.br
 *
 *  (Objetivo) Validar o funcionamento do m�dulo DAC do Projeto XM700
 */

#include "LPC17xx.h"

// Function to provide short delay
void delay(int n)
{
	int d;
	for (d=0; d<n*10; d++);
}

// MAIN function
int main(void) {

	// Set up board/processor
	SystemInit();

	// Configurando portais... 
	LPC_GPIO1->FIODIR &= ~(1<<28);
	LPC_GPIO0->FIODIR |= (1<<21);

	// Enter an infinite loop, cycling through led flash sequence
	while(1) {
		LPC_GPIO0->FIOPIN |= (1<<21);
		delay(1);
		LPC_GPIO0->FIOPIN &= ~(1<<21);
		delay(1);
	}
	return 0 ;
}




