/**
 * \file  serial.c
 * \brief  Biblioteca de comandos das comunica��es seriais (232/485)
 * \author Marcelo Martins Maia do Couto
 * \version 1.0
 * \date  Inicio: 25/Ago/2010
 * \date  Final: 25/Ago/2010
 * \todo 
 * \li Adaptar todos os c�digos para o Doxygen....
 *
 *  (Inicio) Defini��es de comandos para os perif�ricos RS232/RS485
 */

#define LSR_RDR         0x01
#define LSR_OE          0x02
#define LSR_PE          0x04
#define LSR_FE          0x08
#define LSR_BI          0x10
#define LSR_THRE        0x20
#define LSR_TEMT        0x40
#define LSR_RXFE        0x80

// Inicializando a UART2 - Canal fixo do RS485
void Init485(int cclk, int baudrate)
{
  unsigned long int Fdiv;

  // Configurando a fun��o dos pinos como TXD e RXD
  LPC_PINCON->PINSEL0 &= ~(0x0000000F);
  LPC_PINCON->PINSEL0 |= (0x0000000A);
  LPC_PINCON->PINMODE0 &= ~(0x0000000F);
  LPC_PINCON->PINMODE0 |= (0x00000005);
  // Configurando P0.21 para ser o RS485 EN
  LPC_GPIO2->FIODIR |= (1<<8);
  // Turn on power to UART2
  LPC_SC->PCONP |=  (1<<25);
  // Turn on UART2 peripheral clock
  LPC_SC->PCLKSEL1 &= ~(1<<18); //
  LPC_SC->PCLKSEL1 &= ~(1<<19); // PCLK_UART3 = CCLK / 4 = 25MHz
  // 8 Bits, NO Parity, 1 Stop bit, DLAB=1
  LPC_UART3->LCR = 0x83;
  // Configurando a taxa da comunica��o serial
  if (LPC_SC->PCLKSEL1 & 0x000C0000) Fdiv = ( cclk / 8 ) / (16*baudrate);
  else if (LPC_SC->PCLKSEL1 & 0x00040000) Fdiv = ( cclk / 1 ) / (16*baudrate);
  else if (LPC_SC->PCLKSEL1 & 0x00080000) Fdiv = ( cclk / 2 ) / (16*baudrate);
  else Fdiv = ( cclk / 4 ) / (16*baudrate);

  LPC_UART3->DLM = Fdiv / 256;
  LPC_UART3->DLL = Fdiv % 256;
  // Enable and reset TX and RX FIFO (1 char)
  LPC_UART3->LCR = 0x03;

}
// Function to send character over UART
void W485Char(unsigned char c)
{
  while(!(LPC_UART3->LSR & LSR_THRE));  // Block until tx empty
  LPC_GPIO2->FIOPIN |= (1<<8);
  LPC_UART3->THR = c;
  while(!(LPC_UART3->LSR & LSR_THRE));  // Block until tx empty
  LPC_GPIO2->FIOPIN &= ~(1<<8);
}
// Function to get character from UART
unsigned char R485Char(void)
{
  char c;
  while(!(LPC_UART3->LSR & LSR_RDR));  // Nothing received so just block
  c = LPC_UART3->RBR; // Read Receiver buffer register
  return c;
}
// Function to prints the string out over the UART
void W485Phrase(unsigned char *pcString) {
  int i = 0;
  // loop through until reach string's zero terminator
  while (pcString[i] != 0) {
    W485Char(pcString[i]); // print each character
    i++;
  }
}

// Serial RS232 - Fun��es de comunica��o
void Init232(int cclk, int baudrate){
  unsigned long int Fdiv;

  // Configurando a fun��o dos pinos como TXD e RXD
  LPC_PINCON->PINSEL0 &= ~(0x000000F0);
  LPC_PINCON->PINSEL0 |= (0x00000050);
  LPC_PINCON->PINMODE0 &= ~(0x000000F0);
  LPC_PINCON->PINMODE0 |= (0x00000050);
  // Turn on power to UART2
  LPC_SC->PCONP |=  (1<<3);
  // Turn on UART2 peripheral clock
  LPC_SC->PCLKSEL0 &= ~(1<<7); //
  LPC_SC->PCLKSEL0 &= ~(1<<6); // PCLK_UART0 = CCLK / 4 = 25MHz
  // 8 Bits, NO Parity, 1 Stop bit, DLAB=1
  LPC_UART0->LCR = 0x83;
  // Configurando a taxa da comunica��o serial
  if (LPC_SC->PCLKSEL0 & 0x0000000C) Fdiv = ( cclk / 8 ) / (16*baudrate);
  else if (LPC_SC->PCLKSEL0 & 0x00000004) Fdiv = ( cclk / 1 ) / (16*baudrate);
  else if (LPC_SC->PCLKSEL0 & 0x0000008) Fdiv = ( cclk / 2 ) / (16*baudrate);
  else Fdiv = ( cclk / 4 ) / (16*baudrate);

  LPC_UART0->DLM = Fdiv / 256;
  LPC_UART0->DLL = Fdiv % 256;
  // Enable and reset TX and RX FIFO (1 char)
  LPC_UART0->LCR = 0x03;

}
// Function to send character over UART
void W232Char(unsigned char c)
{
  while(!(LPC_UART0->LSR & LSR_THRE));  // Block until tx empty
  LPC_UART0->THR = c;
  while(!(LPC_UART0->LSR & LSR_THRE));  // Block until tx empty
}
// Function to get character from UART
unsigned char R232Char(void)
{
  char c;
  while(!(LPC_UART0->LSR & LSR_RDR));  // Nothing received so just block
  c = LPC_UART0->RBR; // Read Receiver buffer register
  return c;
}
// Function to prints the string out over the UART
void W232Phrase(unsigned char *pcString)
{
  int i = 0;
  // loop through until reach string's zero terminator
  while (pcString[i] != 0) {
    W232Char(pcString[i]); // print each character
    i++;
  }
}
