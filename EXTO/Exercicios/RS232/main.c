/**
 * \file  main.c
 * \brief  Teste de hardware do XM700
 * \author Marcelo Martins Maia do Couto
 * \version 1.0
 * \date  Inicio: 25/Ago/2010
 * \date  Final: 25/Ago/2010
 * \todo
 * \li Site: www.exsto.com.br
 * \li Email: marcelo@exsto.com.br
 *
 *  (Objetivo) Validar o funcionamento do m�dulo RS232 do Projeto XM700
 */

#include "LPC17xx.h"
#include "serial.c"


int main(void)
{
  unsigned char c;

  // Set up board/processor
  SystemInit(); // cclk = 100MHz

  Init232(100000000, 115200); // Setup UART to 115200 baud

  // Sequencia de mensagens da comunica��o Serial
  W232Phrase("\r\n");
  W232Phrase("================================\r\n");
  W232Phrase("== Teste de Comunicacao RS232 ==\r\n");
  W232Phrase("================================\r\n");

  LPC_GPIO0->FIODIR |= (1<<4);

  // Enter an infinite loop, accessing UART and incrementing a counter
  while(1) {
    c = R232Char();	// wait for next character from terminal
    if (c == '1') LPC_GPIO0->FIOPIN  = ~LPC_GPIO0->FIOPIN;
  }
  return 0;
}
