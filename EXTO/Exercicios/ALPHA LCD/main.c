/**
 * \file  main.c
 * \brief  Teste de hardware do XM700
 * \author Marcelo Martins Maia do Couto
 * \version 1.0
 * \date  Inicio: 25/Ago/2010
 * \date  Final: 25/Ago/2010 
 * \todo 
 * \li Site: www.exsto.com.br
 * \li Email: marcelo@exsto.com.br
 *
 *  (Objetivo) Validar o funcionamento do m�dulo Alphanumeric LCD do Projeto XM700
 */

#include "LPC17xx.h"
#include "lcd.c"


// MAIN function
int main(void) {

   // Set up board/processor
   SystemInit();

   // Inicializando o LCD
   InitLCD();

   // Selecionando a segunda linha do LCD
   SelLCDLine(1);

   // Escrevendo no LCD
   WLCDPhrase("Exsto Tecnologia");

   // Ao infinito e alem...
   while(1);

   return 0;
}
