/**
 * \file  LCD.c
 * \brief  Biblioteca de comandos do LCD 16x2
 * \author Marcelo Martins Maia do Couto
 * \version 1.0
 * \date  Inicio: 25/Ago/2010
 * \date  Final: 25/Ago/2010
 * \todo 
 * \li Adaptar todos os c�digos para o Doxygen....
 *
 *  (Inicio) Defini��es de comandos para o LCD 16x2 do kit XM700
 */

// Rotina de atraso por software
void DelayLCD (int n){
	 volatile int d;
	 for (d=0; d<n*300; d++){}
}

// Inicializa��o do Display
void InitLCD(void){
	unsigned int cmds[] = {0x30,0x30,0x30,0x38,0x0E,0x06,0x01};
	unsigned char a = 0;

	LPC_PINCON->PINMODE4 |= ((0xFC)<<24);

	LPC_GPIO2->FIODIR |= (1<<12);   // Colocando o pino de RS como sa�da
	LPC_GPIO2->FIODIR |= (1<<13);   // Colocando o pino de RS como sa�da
	LPC_GPIO0->FIODIR |= (0xff<<4); // Colocando o barramento de dados do LCD como sa�da...
	LPC_GPIO2->FIOPIN &= ~(1<<12); // Setando o pino de comando P2.12 para zero (CMD)
	
	DelayLCD(1);
	
	while (a < 7){		
		LPC_GPIO0->FIOPIN &= ~((0xFF)<<4);
		LPC_GPIO2->FIOPIN |= (1<<13); // Setando o pino de enable P2.13 do LCD 16x2
		LPC_GPIO0->FIOPIN |= (cmds[a]<<4);
		LPC_GPIO2->FIOPIN &= ~(1<<13); // Zerando o pino de enable P2.13 do LCD 16x2
		DelayLCD(1);
		++a;
	}
	DelayLCD(100);
}

// Seleciona em que linha vai escrever a frase
void SelLCDLine(unsigned char line){
	unsigned int cmds[] = {0x80, 0xC0};
	LPC_GPIO2->FIOPIN &= ~(1<<12); // Setando o pino de comando P2.12 para zero (CMD)
	LPC_GPIO0->FIOPIN &= ~((0xFF)<<4);
	LPC_GPIO2->FIOPIN |= (1<<13); // Setando o pino de enable P2.13 do LCD 16x2
	LPC_GPIO0->FIOPIN |= (cmds[line]<<4);
	LPC_GPIO2->FIOPIN &= ~(1<<13); // Zerando o pino de enable P2.13 do LCD 16x2
	DelayLCD(10);
}

// Escreve um caracter 
void WLCDChar(unsigned char letter){
	LPC_GPIO2->FIOPIN |= (1<<12); // Setando o pino de comando P2.12 para zero (CMD)
	LPC_GPIO0->FIOPIN &= ~((0xFF)<<4);
	LPC_GPIO2->FIOPIN |= (1<<13); // Setando o pino de enable P2.13 do LCD 16x2
	LPC_GPIO0->FIOPIN |= (letter<<4);
	LPC_GPIO2->FIOPIN &= ~(1<<13); // Zerando o pino de enable P2.13 do LCD 16x2
	DelayLCD(10);
}

// Escreve uma linha 
void WLCDPhrase(char phrase[]){
	unsigned char a = 0;
	while (a < 16)
	{
		if (phrase[a] != '\0') WLCDChar(phrase[a]);
		else break;
		++a;
	}
}
