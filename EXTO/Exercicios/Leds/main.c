/**
 * \file  main.c
 * \brief  Teste de hardware do XM700
 * \author Marcelo Martins Maia do Couto
 * \version 1.0
 * \date  Inicio: 25/Ago/2010
 * \date  Final: 25/Ago/2010 
 * \todo 
 * \li Site: www.exsto.com.br
 * \li Email: marcelo@exsto.com.br
 *
 *  (Objetivo) Validar o funcionamento do m�dulo LEDS do Projeto XM700  
 */

#include "LPC17xx.h"

// Configurando os pinos que est�o ligados aos leds
__INLINE static void LED_Config(void) {

  LPC_GPIO0->FIODIR = 0xFFFFFFFF;               /* LEDs PORT1 are Output */
}

// Acendendo os LED's
__INLINE static void LED_On (uint32_t led) {

  LPC_GPIO0->FIOPIN |=  (led);                  /* Turn On  LED */
}

// Apagando os LEDs
__INLINE static void LED_Off (uint32_t led) {

  LPC_GPIO0->FIOPIN &= ~(led);                  /* Turn Off LED */
}

// Function to provide short delay
void short_delay (int n) __attribute__((noinline));

void short_delay(int n)
{
   volatile int d;
   for (d=0; d<n*3000; d++){}
}

// MAIN function
int main(void) {

	int i = 4 ;
	int loop;
	
	// Set up board/processor
	SystemInit();

	// Configurando portais... 
	LED_Config();
	
	// Enter an infinite loop, cycling through led flash sequence
	while(1) {

		// Turn all leds on, then off, 5 times
		for (loop=1; loop < 5; loop++) {
			LED_On ((1<<i));
			short_delay(1000);
			LED_Off ((1<<i));                          		
			short_delay(1000);
		}
		i++;
		if(i > 11) i = 4;
	}
	return 0 ;
}


