/**
 * \file  main.c
 * \brief  Teste de hardware do XM700
 * \author Marcelo Martins Maia do Couto
 * \version 1.0
 * \date  Inicio: 25/Ago/2010
 * \date  Final: 25/Ago/2010 
 * \todo 
 * \li Site: www.exsto.com.br
 * \li Email: marcelo@exsto.com.br
 *
 *  (Objetivo) Validar o funcionamento do m�dulo LEDS do Projeto XM700  
 */

#include "LPC17xx.h"
#include "keyboard.c"

void SSegWrite(unsigned char number)
{
	unsigned int SSegChar[] = {0x3f/*0*/,0x06/*1*/,0x5b/*2*/,0x4f/*3*/
							  ,0x66/*4*/,0x6d/*5*/,0x7c/*6*/,0x07/*7*/
							  ,0x7f/*8*/,0x67/*9*/,0x77/*A*/,0x7c/*B*/
							  ,0x39/*C*/,0x5E/*D*/,0x79/*E*/,0x71/*F*/};
	LPC_GPIO0->FIOPIN = (SSegChar[number] << 4);
}

// MAIN function
int main(void) {

	unsigned char tecla = 0;


	// Set up board/processor
	SystemInit();

	// Configurando portais... 
	LPC_GPIO0->FIODIR |= (0xff << 4);
	LPC_GPIO3->FIODIR |= (0x3 << 25);
	LPC_GPIO4->FIODIR |= (0x3 << 28);

	// Ativando pinos...
	LPC_GPIO3->FIOPIN |= (0x3 << 25);
	LPC_GPIO4->FIOPIN |= (0x3 << 28);

	// Inicializando o teclado
	InitKeyboard();


	while(1) {
		tecla = RKeyboard();
		SSegWrite(tecla);
	}
	return 0 ;
}



