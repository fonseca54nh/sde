/**
 * \file  keyboard.c
 * \brief  Biblioteca de comandos do Teclado
 * \author Marcelo Martins Maia do Couto
 * \version 1.0
 * \date  Inicio: 25/Ago/2010
 * \date  Final: 25/Ago/2010
 * \todo 
 * \li Adaptar todos os códigos para o Doxygen....
 *
 *  (Inicio) Definições de comandos para o teclado do kit XM700
 */

void InitKeyboard(void)
{
	LPC_GPIO2->FIODIR &= ~(0xff);
	LPC_GPIO2->FIODIR |= 0x0f; // Configurando o Portal - Distribuição dos Bits. MSB -> LSB (L3 - L2 - L1 - L0 - C3 - C2 - C1 - C0)
	LPC_PINCON->PINMODE4 |= 0x0000FFFF; //Configurando Pulldowns no Teclado
}

unsigned char RKeyboard(void)
{
	unsigned char keys[] = {0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0a,0x0b,0x0c,0x0d,0x0e,0x0f};
	unsigned char keypressed = 0xff;
	unsigned char pressed = 0;
///////////////////////////////////////////////////////////
	LPC_GPIO2->FIOPIN |= (1); // Analisando a primeira linha da varredura
	LPC_GPIO2->FIOPIN |= (1); // Analisando a primeira linha da varredura
	if (LPC_GPIO2->FIOPIN & (1<<4)){
		pressed = 1; keypressed = keys[1];
	}
	else if (LPC_GPIO2->FIOPIN & (1<<5))
	{
		pressed = 1; keypressed = keys[4];
	}
	else if (LPC_GPIO2->FIOPIN & (1<<6))
	{
		pressed = 1; keypressed = keys[7];
	}
	else if (LPC_GPIO2->FIOPIN & (1<<7))
	{
		pressed = 1; keypressed = keys[14];
	}
	LPC_GPIO2->FIOPIN &= ~(1);
	if (pressed) return keypressed;
/////////////////////////////////////////////////////////
	LPC_GPIO2->FIOPIN |= (1<<1); // Analisando a Segunda linha da varredura
	LPC_GPIO2->FIOPIN |= (1<<1); // Analisando a Segunda linha da varredura
	if (LPC_GPIO2->FIOPIN & (1<<4)){
		pressed = 1; keypressed = keys[2];
	}
	else if (LPC_GPIO2->FIOPIN & (1<<5))
	{
		pressed = 1; keypressed = keys[5];
	}
	else if (LPC_GPIO2->FIOPIN & (1<<6))
	{
		pressed = 1; keypressed = keys[8];
	}
	else if (LPC_GPIO2->FIOPIN & (1<<7))
	{
		pressed = 1; keypressed = keys[0];
	}
	LPC_GPIO2->FIOPIN &= ~(1<<1);
	if (pressed) return keypressed;
/////////////////////////////////////////////////////////
	LPC_GPIO2->FIOPIN |= (1<<2); // Analisando a terceira linha da varredura
	LPC_GPIO2->FIOPIN |= (1<<2); // Analisando a terceira linha da varredura
	if (LPC_GPIO2->FIOPIN0 & 0x10){
		pressed = 1; keypressed = keys[3];
	}
	else if (LPC_GPIO2->FIOPIN0 & 0x20){
		pressed = 1; keypressed = keys[6];
	}
	else if (LPC_GPIO2->FIOPIN0 & 0x40){
		pressed = 1; keypressed = keys[9];
	}
	else if (LPC_GPIO2->FIOPIN0 & 0x80){
		pressed = 1; keypressed = keys[15];
	}
	LPC_GPIO2->FIOPIN &= ~(1<<2);
	if (pressed) return keypressed;
	/////////////////////////////////////////////////////////
	LPC_GPIO2->FIOPIN |= (1<<3); // Analisando a Quarta linha da varredura
	LPC_GPIO2->FIOPIN |= (1<<3); // Analisando a Quarta linha da varredura
	if (LPC_GPIO2->FIOPIN0 & 0x10){
		pressed = 1; keypressed = keys[10];
	}
	else if (LPC_GPIO2->FIOPIN0 & 0x20){
		pressed = 1; keypressed = keys[11];
	}
	else if (LPC_GPIO2->FIOPIN0 & 0x40){
		pressed = 1; keypressed = keys[12];
	}
	else if (LPC_GPIO2->FIOPIN0 & 0x80){
		pressed = 1; keypressed = keys[13];
	}
	LPC_GPIO2->FIOPIN &= ~(1<<3);
	if(pressed) return keypressed;
	//else return keypressed;
}

