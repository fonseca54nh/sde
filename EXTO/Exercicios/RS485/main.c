/**
 * \file  main.c
 * \brief  Teste de hardware do XM700
 * \author Marcelo Martins Maia do Couto
 * \version 1.0
 * \date  Inicio: 25/Ago/2010
 * \date  Final: 25/Ago/2010
 * \todo
 * \li Site: www.exsto.com.br
 * \li Email: marcelo@exsto.com.br
 *
 *  (Objetivo) Validar o funcionamento do m�dulo RS485 do Projeto XM700
 */

#include "LPC17xx.h"
#include "serial.c"

int main(void)
{
  char c;

  // Set up board/processor
  SystemInit(); // cclk = 100MHz

  Init485(100000000, 115200); // Setup UART to 115200 baud

  // Sequencia de mensagens da comunica��o Serial
  W485Phrase("\r\n===============================\r\n");
  W485Phrase("==== Teste de Comunicacao RS485\r\n");
  W485Phrase("===============================\r\n");

  LPC_GPIO0->FIODIR |= (1<<4);

  // Ao infinito e al�m!
  while(1) {
    c = R485Char();	// Esperando os caracteres vindos da porta
    if (c == '1') LPC_GPIO0->FIOPIN  = ~LPC_GPIO0->FIOPIN;
  }
  return 0;
}
