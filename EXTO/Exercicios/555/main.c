/**
 * \file  main.c
 * \brief  Teste de hardware do XM700
 * \author Marcelo Martins Maia do Couto
 * \version 1.0
 * \date  Inicio: 25/Ago/2010
 * \date  Final: 25/Ago/2010 
 * \todo 
 * \li Site: www.exsto.com.br
 * \li Email: marcelo@exsto.com.br
 *
 *  (Objetivo) Validar o funcionamento do m�dulo 555 do Projeto XM700
 */

#include "LPC17xx.h"

// MAIN function
int main(void) {

	// Set up board/processor
	SystemInit();

	// Configurando portais... 
	LPC_GPIO1->FIODIR &= ~(1<<28);
	LPC_GPIO0->FIODIR |= (1<<4);

	// Enter an infinite loop, cycling through led flash sequence
	while(1) {
		if(LPC_GPIO1->FIOPIN & (1<<28)) LPC_GPIO0->FIOPIN |= (1<<4);
		else LPC_GPIO0->FIOPIN &= ~(1<<4);
	}
	return 0 ;
}


