/**
 * \file  main.c
 * \brief  Teste de hardware do XM700
 * \author Marcelo Martins Maia do Couto
 * \version 1.0
 * \date  Inicio: 25/Ago/2010
 * \date  Final: 25/Ago/2010 
 * \todo 
 * \li Site: www.exsto.com.br
 * \li Email: marcelo@exsto.com.br
 *
 *  (Objetivo) Validar o funcionamento do m�dulo LEDS do Projeto XM700  
 */

#include "LPC17xx.h"

// Function to provide short delay
void short_delay (int n) __attribute__((noinline));

void short_delay(int n)
{
   volatile int d;
   for (d=0; d<n*3000; d++){}
}

// MAIN function
int main(void) {

	// Set up board/processor
	SystemInit();

	// Configurando portais... 
	 LPC_GPIO1->FIODIRH = 0x2000;
	
	// Enter an infinite loop, cycling through led flash sequence
	while(1) {
			LPC_GPIO1->FIOPINH = 0x2000;
			short_delay(5);
			LPC_GPIO1->FIOPINH = 0x0000;
			short_delay(5);
	}
	return 0 ;
}


